define ["tiles", "controls"], (Tiles) ->

  class World extends createjs.Container

    constructor: (@map) ->
      super()
      tiles = new Tiles(@map)
      @width = tiles.width
      @height = tiles.height
      @staticObjects = []
      @addChild(tiles.container)

      @x = WIDTH/2
      @y = HEIGHT/2

      @regX = @width/2
      @regY = @height/2

      @initStaticObj()

      createjs.Ticker.addEventListener("tick", ((e)=>@update(e)))

    initStaticObj: ->
      for layer in @map.layers
        do (layer) =>
          return unless layer.type == "objectgroup"
          for object in layer.objects
            do (object) =>
              @staticObjects.push object
##              Debug :
#              body = new createjs.Shape(new createjs.Graphics().beginFill("green").drawRect(object.x, object.y, object.width, object.height))
#              body.alpha = 0.5
#              @addChild body
##

    update: (e) ->
      y = HEIGHT/2-@player.y+@height/2
      x = WIDTH/2-@player.x+@width/2
      @y = y if HEIGHT - y < @height/2 and y < @height/2
      @x= x if WIDTH - x < @width/2 and x < @width/2

