define ["socket", "world", "player"], (Socket, World, Player) ->

  class Game

    constructor: (@canvas) ->
      createjs.Ticker.setFPS(FPS)
      createjs.Touch.enable(@stage)

      @updater = 50

      @stage = new createjs.Stage(@canvas)
      @stage.enableMouseOver(10)
      createjs.Touch.enable @stage

      @container = new createjs.Container

      @placeholder = new createjs.Shape(new createjs.Graphics().beginFill("rgba(0,0,0,0.5)").drawRect(0,0, WIDTH, HEIGHT))
      @placeholder.visible = false
      @placeholder.cache 0,0, WIDTH, HEIGHT

      @bg = new createjs.Shape(new createjs.Graphics().beginFill("#52b4f0").drawRect(0,0, WIDTH, HEIGHT))
      @bg.cache 0,0, WIDTH, HEIGHT

      @stage.addChild @bg, @container, @placeholder

      @debugOn()

      @socketWrapper = new Socket(@)

    createWorld: (params) ->
      @world = new World params.world
      @createPlayer(params.player)
      @world.player = @player
      @container.addChild @world
      @world

    createPlayer: (params) ->
      @player = new Player(params, @, true)
      @world.addChild @player

      createjs.Ticker.addEventListener("tick", ((e)=>@handleTick(e)))

      @player

    debugOn: ->
      @stats = new Stats()

    pause: ->
      createjs.Ticker.setPaused true
      @placeholder.visible = true
      @stage.update()

    play:  ->
      createjs.Ticker.setPaused false
      @placeholder.visible = false

    handleTick: (e) ->
      return unless @player

      @stats.begin()
      @stage.update() unless createjs.Ticker.getPaused()
      @updater -= e.delta if @updater > 0
      @updater = 0 if @updater < 0

      if @updater == 0
        @updater = 50
        @socketWrapper.send({cmd:"upd", params: {player: @player.toJSON()} })

      @stats.end()
