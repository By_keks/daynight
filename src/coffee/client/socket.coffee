
define ["player"], (Player) ->

  class Socket

    constructor: (@game) ->
      @players = {}

      server = "ws://185.20.227.221:8001"
#      server = "ws:/localhost:8001"
      @socket = new WebSocket(server)

      @socket.onopen = =>
        console.log("Соединение установлено.")
        @send({cmd: "signIn", params: nick: localStorage.getItem("nick"), pid: null})

      @socket.onclose = (event) ->
        if event.wasClean
          console.log('Соединение закрыто чисто')
        else
          console.log('Обрыв соединения')
        console.log('Код: ' + event.code + ' причина: ' + event.reason)

      @socket.onmessage = (event) =>
        data = JSON.parse(event.data)
        @[data.cmd](data.params) if _.isFunction(@[data.cmd])

      @socket.onerror = (error) ->
        console.log("Ошибка " + error.message)

    send: (data) ->@socket.send(JSON.stringify(data))

    signIn: (params) ->
      if params.status == "success"
        @game.createWorld(params)

    signOut: (params) ->
      @players[params.player.pid].destroy()
      delete @players[params.player.pid]

    upd: (params) ->

      return if _.isUndefined(@game.player)
      _.each params.players, (player) =>

        x =  player.x
        y = player.y

        delete player.x
        delete player.y

        if parseInt(player.pid) == parseInt(@game.player.pid)
          delete player.direction
          delete player.color
          delete player.team
          @game.player.parse(player)
        else
          unless _.has @players, player.pid
            @players[player.pid] = new Player(player, @game, false)
            @game.world.addChild @players[player.pid]

          @players[player.pid].parse(player)

          return if parseInt(@players[player.pid].x) == parseInt(x) and parseInt(@players[player.pid].y) == parseInt(y)
          createjs.Tween.removeTweens(@players[player.pid]) if @players[player.pid].tween
          @players[player.pid].tween = createjs.Tween.get(@players[player.pid]).to({x: x, y: y}, 50)
