define ->

  KEYS =
    LEFT     : 37
    RIGHT    : 39
    SPACEBAR : 32
    UP       : 38
    DOWN     : 40
    A        : 65
    D        : 68
    W        : 87
    S        : 83

  class Controls

    left: false
    right: false
    up: false
    down: false
    jumping: false

#    buttons:
#      A: 0
#      B: 1
#      X: 2
#      Y: 3
#
#      LTS: 4
#      LTS: 5
#
#      SELECT: 6
#      START: 7
#
#    axes:
#      DPRL: 6
#      DPTB: 7


    constructor: ->
      @eventDispatcher()

      createjs.Ticker.addEventListener("tick", ((e)=>@update(e)))

    eventDispatcher: ->
      document.onkeydown = @onControlDown
      document.onkeyup   = @onControlUp

    onControlDown: (e) =>
      e.preventDefault()
      switch e.which
        when KEYS.LEFT, KEYS.A
          @left = true
        when KEYS.RIGHT, KEYS.D
          @right = true
        when KEYS.SPACEBAR, KEYS.W, KEYS.UP
          @up = true
        when KEYS.DOWN, KEYS.S
          @down = true

    onControlUp: (e) =>
      e.preventDefault()
      switch e.which
        when KEYS.LEFT, KEYS.A
          @left = false
        when KEYS.RIGHT, KEYS.D
          @right = false
        when KEYS.SPACEBAR, KEYS.W, KEYS.UP
          @up = false
        when KEYS.DOWN, KEYS.S
          @down = false

    update: ->
#      @pads = navigator.getGamepads()
#      return if @pads.length <= 0
#      @up = if @pads[0].buttons[@buttons.A].pressed then true else false
#      if @pads[0].axes[@axes.DPRL] > 0
#        @right = true
#      else if @pads[0].axes[@axes.DPRL] < 0
#        @left = true
#      else
#        @left = @right = false

  new Controls