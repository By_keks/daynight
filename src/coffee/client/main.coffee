requirejs.config
  baseUrl: '/js/client/'

WIDTH = 1000
HEIGHT = 500

SCALE = 50

FPS = 60

$ ->
  init = ->
    if _.isNull localStorage.getItem('nick')
      $(".sign_in").show()
      $(".sign_in input:button").on 'click', (e) ->
        return $(".sign_in").css({color: "#F0142C"}) if _.isEmpty $("#nick").val()
        localStorage.setItem('nick', $("#nick").val())
        init()
      true
    else
      $(".sign_in").hide()

      require ["game"], (Game) ->
        canvas = $("<canvas id='canvas' unselectable='on' width='1200' height='750'></canvas>")

        game = new Game( $('.wrapper').append(canvas).find('canvas')[0])

        $(window).on 'blur', ((e) -> game.pause())
        $(window).on 'focus', ((e) -> game.play())

        document.body.appendChild( game.stats.domElement )
        game.stats.domElement.style.position = "absolute"
        game.stats.domElement.style.top = "0px"

        $("#fs").on 'click', (e) ->
          e.preventDefault()
          $("canvas")[0].mozRequestFullScreen()

        canvas.attr({width:WIDTH, height:HEIGHT})
        $('.wrapper').width(WIDTH)

  init()
