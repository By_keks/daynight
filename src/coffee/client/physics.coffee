define ["controls"], (Controls) ->

  class Physics extends createjs.Container

    constructor: (params, @game, self = false) ->
      @speed = 7
      @gravity = 5
      @jumpImpulse = 13.5
      @lastJump = 0
      @g = 0.2

      @frictionY = 0.25
      @frictionXY = 0.50
      @frictionX = 2

      @vectorY = 0
      @vectorX = 0

      @impulseY = 0
      @impulseX = 0

      @target = null

      super

    update: (e) ->
      @lastJump -= e.delta if @lastJump > 0
      @lastJump = 0 if @lastJump < 0

      @py = @y
      @px = @x

      @collisionHandler()

      @y = @py
      @x = @px

      if Controls.up and @isNotJump()
        @lastJump = 1000
        @impulseY = -@jumpImpulse
        @impulseX = if @impulseX < 0 then @impulseX-2 else if @impulseX > 0 then @impulseX+2 else 0

      if Controls.right
        if @isNotJump()
          @impulseX = @speed
        else if @impulseX <= 0
          @impulseX = @speed/2

      if Controls.left
        if @isNotJump()
          @impulseX = -@speed
        else if @impulseX >= 0
          @impulseX = -@speed/2
      @direction = if @impulseX < 0 then -1 else if @impulseX > 0 then 1 else @direction

    getXForWorld: (x) -> x - @game.world.x
    getYForWorld: (y) -> y - @game.world.y


    isNotJump: -> @vectorY == 0 and @lastJump == 0

    collisionHandler: (obj) ->

      if @impulseY < 0 then @impulseY += @frictionY else if @impulseY > 0 then @impulseY -= @frictionY
      @impulseY = 0 if @impulseY > -@frictionY and @impulseY < @frictionY
      @vectorY = @impulseY+@gravity
      @vectorX = @impulseX

      for obj in @game.world.staticObjects
        do (obj) =>

          deltaX = if @vectorX > 0 then obj.x - @x-@width/2 else obj.x+obj.width - @x+@width/2
          @vectorX = @getVectorByX(deltaX, obj)

          if @vectorY > 0

            deltaY = obj.y - @y-@height/2
            return if deltaY < 0
            if deltaY == 0
              @gravity = 5
              @lastJump = 0

            isX = @x+@width/2 > obj.x and @x-@width/2 < obj.x+obj.width
            @vectorY = deltaY if deltaY < @vectorY and isX

          else if @vectorY < 0

            deltaY = obj.y+obj.height - @y+@height/2

            return if deltaY < 0

            if deltaY < @vectorY*-1 and @x+@width/2 > obj.x and @x-@width/2 < obj.x+obj.width
              @gravity = 5
              @impulseY = 0
              @lastJump = 0
              @vectorY = deltaY

      @gravity += @g if @vectorY >= 0

      if @isNotJump()
        if @impulseX < 0 then @impulseX += @frictionX else if @impulseX > 0 then @impulseX -= @frictionX

      @impulseX = 0 if @impulseX > -@frictionX and @impulseX < @frictionX

      @py += @vectorY
      @px += @vectorX

    getVectorByX: (deltaX, obj) ->
      return @vectorX if deltaX < 0
      @vectorX = deltaX if deltaX < @speed and @y+@height/2 > obj.y and @y-@height/2 < obj.y+obj.height
      @impulseX = 0 if @vectorX == 0
      @vectorX