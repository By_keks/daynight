define ["physics", "controls"], (Physics, Controls) ->

  class Player extends Physics

    constructor: (params, @game, @self = false) ->

      @seters =
        hp: (val) => @setHP(val)
        pid: (val) => @pid = parseInt(val)
        team: (val) => @team = parseInt(val)

      @scale = 0.3
      @direction = 1
      @isAttacks = false
      @lock = false

      super
      @parse(params)
      @render()
      @delegateEvents() if @self

      createjs.Ticker.addEventListener("tick", ((e)=>@update(e)))

    parse: (params) ->
      _.each params, (val, name) =>
        return @seters[name](val) if _.has @seters, name
        @[name] = val
        return

    toJSON: ->
      id: @pid
      x: @x
      y: @y
      direction: @direction

    setHP: (val) ->
      return if @hp == val
      @hpChange(val) unless _.isUndefined(@hp)
      @hp = val
      @setName(@name, false)
      return

    hpChange: (val) ->
      hp = new createjs.Container
      text = @title.clone()
      if @hp > val
        text.text = "-#{@hp-val}"
        text.color = "red"
      else
        text.text = "+#{val-@hp}"
        text.color = "green"
      text.x = 0
      hp.addChild text
      @addChild hp
      hp.cache -20, -25, 100, 100
      createjs.Tween.get(hp).to({y: hp.y-100}, 1000).call( => @removeChild(hp))

    delegateEvents: ->
      @game.stage.on 'stagemousedown', (e) =>
        @mouseX = @getXForWorld(parseInt(e.stageX))
        @mouseY = @getYForWorld(parseInt(e.stageY))
        @attackOn()

      @game.stage.on 'stagemouseup', (e) =>
        @attackOff()

    spineComplete: ->
      @spine.state.setAnimationByName(0, @spine.state.data.skeletonData.animations[0].name, true)

    render: ->
      @spine = new createjs.Spine("res/sceletons/ninja/speedy.atlas", "res/sceletons/ninja/speedy.json")
#      @spine = new createjs.Spine("res/sceletons/viking/skeleton.atlas", "res/sceletons/viking/skeleton.json")
#      @spine = new createjs.Spine("res/sceletons/dragon/dragon.atlas", "res/sceletons/dragon/dragon.json")
#      @spine = new createjs.Spine("res/sceletons/goblins/goblins.atlas", "res/sceletons/goblins/goblins.json")
#      @spine = new createjs.Spine("res/sceletons/hero/hero.atlas", "res/sceletons/hero/hero.json")
#      @spine = new createjs.Spine("res/sceletons/powerup/powerup.atlas", "res/sceletons/powerup/powerup.json")
      @spine.scaleX = @spine.scaleY = @scale
      @spine.x = @width/2
      @spine.y = @height
      @spine.onComplete = => @spineComplete()

      @regX = @width/2
      @regY = @height/2

      @startX = @x
      @startY = @y

      @title = new createjs.Text("", "20px Arial", @color)
      @title.textBaseline = "alphabetic"

      @setName(@name)
      @addChild @spine , @title

    setName: (name, change = true) ->
      return unless @title
      @name = name if change
      @title.text = "#{name} HP:#{@hp}"
      @title.x = -@title.getMeasuredWidth()/2+@width/2
      @title.y = -10
      @title.cache -20, -20, 200, 300

    setTarget: (x, y) ->
      targets = []
      _targets = []

      _.each @game.socketWrapper.players, (player) =>
        return if player.team == @team
        deltaX = @x-player.x
        deltaX *= -1 if deltaX < 0
        deltaY = @y-player.y
        deltaY *= -1 if deltaY < 0

        targets.push player if deltaX <= @attackRadiusX and deltaY <= @attackRadiusY

      for player in targets
        do (player) =>

          deltaX = x - player.x
          deltaX *= -1 if deltaX < 0
          deltaY = y - player.y
          deltaY *= -1 if deltaY < 0

          _targets.push {deltaX: deltaX , deltaY: deltaY, target: player}

      _targets.sort ((body1, body2) ->
        weight = 0
        weight += if body1.deltaX < body2.deltaX then -1 else if body1.deltaX > body2.deltaX then 1 else 0
        weight += if body1.deltaY < body2.deltaY then -1 else if body1.deltaY > body2.deltaY then 1 else 0
        return weight
      )

      @target = if _targets.length > 0 then _.first(_targets).target else null

    attackOn: -> @isAttacks = true
    attackOff: ->  @isAttacks = false

    sendAttack: ->
      return if @lockAttack
      @setTarget @mouseX, @mousey
      return unless @target
      @game.socketWrapper.send({cmd:"attk", params: {player: @pid, target:@target.pid}})

    destroy: -> @game.world.removeChild @

    update: (e) ->
      return if @isDead
      @sendAttack() if @isAttacks
      @spine.scaleX = @scale*@direction
      @spine.update(e)
      super(e) if @self