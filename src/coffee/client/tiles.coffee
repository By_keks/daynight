define ->

  class Tiles

    constructor: (@map) ->
      @container = new createjs.Container
      @queue = new createjs.LoadQueue(true)
      @queue.addEventListener("complete", => @complete())
      @queue.loadManifest([ { src: 'res/map.png',  id: 'map.png' } ])

      @layers = {}

      @tilewidth  = @map.tilesets[0].tilewidth
      @tileheight = @map.tilesets[0].tileheight

      @width = @tilewidth*@map.width
      @height = @tileheight*@map.height

    complete: ->
      data =
        images: [ @queue.getResult(@map.tilesets[0].image) ]
        frames: {
          width:    @tilewidth
          height:   @tileheight
          spacing:  @map.tilesets[0].spacing
          margin:   @map.tilesets[0].margin
        }

      sprite = new createjs.Sprite(new createjs.SpriteSheet(data))
      @render(sprite)

    getLayerByName: (name) -> @layers[name]

    render: (sprite) ->
      for layer in @map.layers
        do (layer) =>
          return unless layer.type == "tilelayer"

          container = new createjs.Container
          container.name = layer.name
          layer.container = container

          @layers[layer.name] = layer

          wi = layer.width
          hi = layer.height

          x = 0
          y = 0

          for key in layer.data
            do (key) =>
              if wi <= 0
                wi = layer.width
                hi -= 1
                y += @tileheight
                x = 0

              tile = sprite.clone()
              tile.name = key

              tile.x += x
              tile.y += y
              tile.gotoAndStop(key-1)
              container.addChild tile

              x += @tilewidth
              wi -= 1

          container.cache 0,0, @width, @height
          @container.addChild container