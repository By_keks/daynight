fs = require('fs')

class World

  constructor: ->
    @map = JSON.parse(fs.readFileSync('./../../res/map.json', 'utf8'))
    @width = @map.width*@map.tilewidth
    @height = @map.height*@map.tileheight

  toJSON: -> @map

module.exports = new World

