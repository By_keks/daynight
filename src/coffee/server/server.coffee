ws = require("nodejs-websocket")
global._ = require("lodash")
global.World = require("./world")

Connect = require("./connect")

module.exports = class Server

  constructor: ->
    @players = {}
    @server = ws.createServer(((conn)=>@connection(conn))).listen(8001)
    setInterval(( => @send({cmd: "upd", params: { players: @players } }) ), 50)

  connection: (conn) -> new Connect(conn, @)

  send: (data) ->
    @server.connections.forEach(
      (connect) => connect.sendText(JSON.stringify(data))
    )