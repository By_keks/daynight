module.exports = class Player

  constructor: (@connect) ->
    @default =
      team: 0
      hp: 100
      width: 60
      height: 90
      damage: 20
      color: "red"
      direction: 1
      isDead: false
      lockAttack: false
      x: World.width/2
      y: World.height/2
      attackRadiusX: 80
      attackRadiusY: 100

    @params = _.clone(@default)

    @isDead = false
    @timeoutToResp = 3000
    @lastAttack = 1000
    @target = null

  toJSON: -> @params

  parse: (params) ->
    _.each params, (val, name) =>
      parseInt(val) if name == "pid"
      @params[name] = val

  getsHit: (player) ->
    return if @params.isDead
    hp = @params.hp
    hp -= player.params.damage
    @params.isDead = true if hp <= 0
    clearTimeout(@timerToResp)
    @timerToResp = setTimeout( (=> @resp()), 5000 ) if @params.isDead
    @params.hp = hp

    impulseX = if @params.x > player.params.x then 5 else if @params.x < player.params.x then -5 else 0

    params = _.extend(_.clone(@toJSON()), { impulseX: impulseX, impulseY: -10 })
    @connect.send({cmd: "upd", params: { players: [params] } })

  resp: ->
    @params.x = @default.x
    @params.y = @default.y
    @params.hp = @default.hp
    @params.isDead = @default.isDead

  attack: (target) ->
    return if @params.lockAttack
    target.getsHit(@)
    @params.lockAttack = true
    setTimeout((=> @params.lockAttack = false), 1000)

  switchTeam: ->
    @params.team = if @params.team == 0 then 1 else 0
    @params.color = if @params.team == 0 then "red" else "blue"