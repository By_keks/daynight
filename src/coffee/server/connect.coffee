Player = require("./player")

module.exports = class Connect

  params:
    pid: 0
    pids: []
    teams:
      0: 0
      1: 0

  constructor: (conn, @server) ->
    console.log("New connection")
    @open = true
    @conn = conn
    @conn.on "text", ((str) => @text(str))
    @conn.on "close", (=> @close())
    @

  close: ->
    pid = @player.params.pid
    @params.teams[@player.params.team] -= 1
    console.log "Player #{pid} goodbye"
    @server.send {cmd:"signOut", params:{ player: @player.toJSON() }}
    @server.players[pid] = null
    delete @server.players[pid]

  send: (data) ->
    @conn.sendText(JSON.stringify(data))

  text: (str) ->
    data = JSON.parse(str)
    @[data.cmd](data.params) if _.isFunction(@[data.cmd])

  signIn: (params) ->
    pid = if _.isEmpty(params.pid) then @params.pid += 1 else params.pid
    @player = new Player(@)
    @player.params.pid = pid
    @player.params.name = params.nick
    @server.players[@player.params.pid] = @player
    @player.switchTeam() if @params.teams[0] > @params.teams[1]
    @params.teams[@player.params.team] += 1
    console.log "Player #{pid} join to #{@player.params.color} team"
    @send {cmd:"signIn", params:{ player: @player.toJSON(), world: World.toJSON(), status: "success" }}

  upd: (params) -> @player.parse params.player

  attk: (params) ->
    @player.attack(@server.players[params.target])