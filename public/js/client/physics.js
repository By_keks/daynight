var __hasProp = {}.hasOwnProperty,
  __extends = function(child, parent) { for (var key in parent) { if (__hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; };

define(["controls"], function(Controls) {
  var Physics;
  return Physics = (function(_super) {
    __extends(Physics, _super);

    function Physics(params, game, self) {
      this.game = game;
      if (self == null) {
        self = false;
      }
      this.speed = 7;
      this.gravity = 5;
      this.jumpImpulse = 13.5;
      this.lastJump = 0;
      this.g = 0.2;
      this.frictionY = 0.25;
      this.frictionXY = 0.50;
      this.frictionX = 2;
      this.vectorY = 0;
      this.vectorX = 0;
      this.impulseY = 0;
      this.impulseX = 0;
      this.target = null;
      Physics.__super__.constructor.apply(this, arguments);
    }

    Physics.prototype.update = function(e) {
      if (this.lastJump > 0) {
        this.lastJump -= e.delta;
      }
      if (this.lastJump < 0) {
        this.lastJump = 0;
      }
      this.py = this.y;
      this.px = this.x;
      this.collisionHandler();
      this.y = this.py;
      this.x = this.px;
      if (Controls.up && this.isNotJump()) {
        this.lastJump = 1000;
        this.impulseY = -this.jumpImpulse;
        this.impulseX = this.impulseX < 0 ? this.impulseX - 2 : this.impulseX > 0 ? this.impulseX + 2 : 0;
      }
      if (Controls.right) {
        if (this.isNotJump()) {
          this.impulseX = this.speed;
        } else if (this.impulseX <= 0) {
          this.impulseX = this.speed / 2;
        }
      }
      if (Controls.left) {
        if (this.isNotJump()) {
          this.impulseX = -this.speed;
        } else if (this.impulseX >= 0) {
          this.impulseX = -this.speed / 2;
        }
      }
      return this.direction = this.impulseX < 0 ? -1 : this.impulseX > 0 ? 1 : this.direction;
    };

    Physics.prototype.getXForWorld = function(x) {
      return x - this.game.world.x;
    };

    Physics.prototype.getYForWorld = function(y) {
      return y - this.game.world.y;
    };

    Physics.prototype.isNotJump = function() {
      return this.vectorY === 0 && this.lastJump === 0;
    };

    Physics.prototype.collisionHandler = function(obj) {
      var _fn, _i, _len, _ref;
      if (this.impulseY < 0) {
        this.impulseY += this.frictionY;
      } else if (this.impulseY > 0) {
        this.impulseY -= this.frictionY;
      }
      if (this.impulseY > -this.frictionY && this.impulseY < this.frictionY) {
        this.impulseY = 0;
      }
      this.vectorY = this.impulseY + this.gravity;
      this.vectorX = this.impulseX;
      _ref = this.game.world.staticObjects;
      _fn = (function(_this) {
        return function(obj) {
          var deltaX, deltaY, isX;
          deltaX = _this.vectorX > 0 ? obj.x - _this.x - _this.width / 2 : obj.x + obj.width - _this.x + _this.width / 2;
          _this.vectorX = _this.getVectorByX(deltaX, obj);
          if (_this.vectorY > 0) {
            deltaY = obj.y - _this.y - _this.height / 2;
            if (deltaY < 0) {
              return;
            }
            if (deltaY === 0) {
              _this.gravity = 5;
              _this.lastJump = 0;
            }
            isX = _this.x + _this.width / 2 > obj.x && _this.x - _this.width / 2 < obj.x + obj.width;
            if (deltaY < _this.vectorY && isX) {
              return _this.vectorY = deltaY;
            }
          } else if (_this.vectorY < 0) {
            deltaY = obj.y + obj.height - _this.y + _this.height / 2;
            if (deltaY < 0) {
              return;
            }
            if (deltaY < _this.vectorY * -1 && _this.x + _this.width / 2 > obj.x && _this.x - _this.width / 2 < obj.x + obj.width) {
              _this.gravity = 5;
              _this.impulseY = 0;
              _this.lastJump = 0;
              return _this.vectorY = deltaY;
            }
          }
        };
      })(this);
      for (_i = 0, _len = _ref.length; _i < _len; _i++) {
        obj = _ref[_i];
        _fn(obj);
      }
      if (this.vectorY >= 0) {
        this.gravity += this.g;
      }
      if (this.isNotJump()) {
        if (this.impulseX < 0) {
          this.impulseX += this.frictionX;
        } else if (this.impulseX > 0) {
          this.impulseX -= this.frictionX;
        }
      }
      if (this.impulseX > -this.frictionX && this.impulseX < this.frictionX) {
        this.impulseX = 0;
      }
      this.py += this.vectorY;
      return this.px += this.vectorX;
    };

    Physics.prototype.getVectorByX = function(deltaX, obj) {
      if (deltaX < 0) {
        return this.vectorX;
      }
      if (deltaX < this.speed && this.y + this.height / 2 > obj.y && this.y - this.height / 2 < obj.y + obj.height) {
        this.vectorX = deltaX;
      }
      if (this.vectorX === 0) {
        this.impulseX = 0;
      }
      return this.vectorX;
    };

    return Physics;

  })(createjs.Container);
});
