define(["socket", "world", "player"], function(Socket, World, Player) {
  var Game;
  return Game = (function() {
    function Game(canvas) {
      this.canvas = canvas;
      createjs.Ticker.setFPS(FPS);
      createjs.Touch.enable(this.stage);
      this.updater = 50;
      this.stage = new createjs.Stage(this.canvas);
      this.stage.enableMouseOver(10);
      createjs.Touch.enable(this.stage);
      this.container = new createjs.Container;
      this.placeholder = new createjs.Shape(new createjs.Graphics().beginFill("rgba(0,0,0,0.5)").drawRect(0, 0, WIDTH, HEIGHT));
      this.placeholder.visible = false;
      this.placeholder.cache(0, 0, WIDTH, HEIGHT);
      this.bg = new createjs.Shape(new createjs.Graphics().beginFill("#52b4f0").drawRect(0, 0, WIDTH, HEIGHT));
      this.bg.cache(0, 0, WIDTH, HEIGHT);
      this.stage.addChild(this.bg, this.container, this.placeholder);
      this.debugOn();
      this.socketWrapper = new Socket(this);
    }

    Game.prototype.createWorld = function(params) {
      this.world = new World(params.world);
      this.createPlayer(params.player);
      this.world.player = this.player;
      this.container.addChild(this.world);
      return this.world;
    };

    Game.prototype.createPlayer = function(params) {
      this.player = new Player(params, this, true);
      this.world.addChild(this.player);
      createjs.Ticker.addEventListener("tick", ((function(_this) {
        return function(e) {
          return _this.handleTick(e);
        };
      })(this)));
      return this.player;
    };

    Game.prototype.debugOn = function() {
      return this.stats = new Stats();
    };

    Game.prototype.pause = function() {
      createjs.Ticker.setPaused(true);
      this.placeholder.visible = true;
      return this.stage.update();
    };

    Game.prototype.play = function() {
      createjs.Ticker.setPaused(false);
      return this.placeholder.visible = false;
    };

    Game.prototype.handleTick = function(e) {
      if (!this.player) {
        return;
      }
      this.stats.begin();
      if (!createjs.Ticker.getPaused()) {
        this.stage.update();
      }
      if (this.updater > 0) {
        this.updater -= e.delta;
      }
      if (this.updater < 0) {
        this.updater = 0;
      }
      if (this.updater === 0) {
        this.updater = 50;
        this.socketWrapper.send({
          cmd: "upd",
          params: {
            player: this.player.toJSON()
          }
        });
      }
      return this.stats.end();
    };

    return Game;

  })();
});
