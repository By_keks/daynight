var __hasProp = {}.hasOwnProperty,
  __extends = function(child, parent) { for (var key in parent) { if (__hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; };

define(["tiles", "controls"], function(Tiles) {
  var World;
  return World = (function(_super) {
    __extends(World, _super);

    function World(map) {
      var tiles;
      this.map = map;
      World.__super__.constructor.call(this);
      tiles = new Tiles(this.map);
      this.width = tiles.width;
      this.height = tiles.height;
      this.staticObjects = [];
      this.addChild(tiles.container);
      this.x = WIDTH / 2;
      this.y = HEIGHT / 2;
      this.regX = this.width / 2;
      this.regY = this.height / 2;
      this.initStaticObj();
      createjs.Ticker.addEventListener("tick", ((function(_this) {
        return function(e) {
          return _this.update(e);
        };
      })(this)));
    }

    World.prototype.initStaticObj = function() {
      var layer, _i, _len, _ref, _results;
      _ref = this.map.layers;
      _results = [];
      for (_i = 0, _len = _ref.length; _i < _len; _i++) {
        layer = _ref[_i];
        _results.push((function(_this) {
          return function(layer) {
            var object, _j, _len1, _ref1, _results1;
            if (layer.type !== "objectgroup") {
              return;
            }
            _ref1 = layer.objects;
            _results1 = [];
            for (_j = 0, _len1 = _ref1.length; _j < _len1; _j++) {
              object = _ref1[_j];
              _results1.push((function(object) {
                return _this.staticObjects.push(object);
              })(object));
            }
            return _results1;
          };
        })(this)(layer));
      }
      return _results;
    };

    World.prototype.update = function(e) {
      var x, y;
      y = HEIGHT / 2 - this.player.y + this.height / 2;
      x = WIDTH / 2 - this.player.x + this.width / 2;
      if (HEIGHT - y < this.height / 2 && y < this.height / 2) {
        this.y = y;
      }
      if (WIDTH - x < this.width / 2 && x < this.width / 2) {
        return this.x = x;
      }
    };

    return World;

  })(createjs.Container);
});
