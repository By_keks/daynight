var __bind = function(fn, me){ return function(){ return fn.apply(me, arguments); }; };

define(function() {
  var Controls, KEYS;
  KEYS = {
    LEFT: 37,
    RIGHT: 39,
    SPACEBAR: 32,
    UP: 38,
    DOWN: 40,
    A: 65,
    D: 68,
    W: 87,
    S: 83
  };
  Controls = (function() {
    Controls.prototype.left = false;

    Controls.prototype.right = false;

    Controls.prototype.up = false;

    Controls.prototype.down = false;

    Controls.prototype.jumping = false;

    function Controls() {
      this.onControlUp = __bind(this.onControlUp, this);
      this.onControlDown = __bind(this.onControlDown, this);
      this.eventDispatcher();
      createjs.Ticker.addEventListener("tick", ((function(_this) {
        return function(e) {
          return _this.update(e);
        };
      })(this)));
    }

    Controls.prototype.eventDispatcher = function() {
      document.onkeydown = this.onControlDown;
      return document.onkeyup = this.onControlUp;
    };

    Controls.prototype.onControlDown = function(e) {
      e.preventDefault();
      switch (e.which) {
        case KEYS.LEFT:
        case KEYS.A:
          return this.left = true;
        case KEYS.RIGHT:
        case KEYS.D:
          return this.right = true;
        case KEYS.SPACEBAR:
        case KEYS.W:
        case KEYS.UP:
          return this.up = true;
        case KEYS.DOWN:
        case KEYS.S:
          return this.down = true;
      }
    };

    Controls.prototype.onControlUp = function(e) {
      e.preventDefault();
      switch (e.which) {
        case KEYS.LEFT:
        case KEYS.A:
          return this.left = false;
        case KEYS.RIGHT:
        case KEYS.D:
          return this.right = false;
        case KEYS.SPACEBAR:
        case KEYS.W:
        case KEYS.UP:
          return this.up = false;
        case KEYS.DOWN:
        case KEYS.S:
          return this.down = false;
      }
    };

    Controls.prototype.update = function() {};

    return Controls;

  })();
  return new Controls;
});
