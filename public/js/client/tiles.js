define(function() {
  var Tiles;
  return Tiles = (function() {
    function Tiles(map) {
      this.map = map;
      this.container = new createjs.Container;
      this.queue = new createjs.LoadQueue(true);
      this.queue.addEventListener("complete", (function(_this) {
        return function() {
          return _this.complete();
        };
      })(this));
      this.queue.loadManifest([
        {
          src: 'res/map.png',
          id: 'map.png'
        }
      ]);
      this.layers = {};
      this.tilewidth = this.map.tilesets[0].tilewidth;
      this.tileheight = this.map.tilesets[0].tileheight;
      this.width = this.tilewidth * this.map.width;
      this.height = this.tileheight * this.map.height;
    }

    Tiles.prototype.complete = function() {
      var data, sprite;
      data = {
        images: [this.queue.getResult(this.map.tilesets[0].image)],
        frames: {
          width: this.tilewidth,
          height: this.tileheight,
          spacing: this.map.tilesets[0].spacing,
          margin: this.map.tilesets[0].margin
        }
      };
      sprite = new createjs.Sprite(new createjs.SpriteSheet(data));
      return this.render(sprite);
    };

    Tiles.prototype.getLayerByName = function(name) {
      return this.layers[name];
    };

    Tiles.prototype.render = function(sprite) {
      var layer, _i, _len, _ref, _results;
      _ref = this.map.layers;
      _results = [];
      for (_i = 0, _len = _ref.length; _i < _len; _i++) {
        layer = _ref[_i];
        _results.push((function(_this) {
          return function(layer) {
            var container, hi, key, wi, x, y, _fn, _j, _len1, _ref1;
            if (layer.type !== "tilelayer") {
              return;
            }
            container = new createjs.Container;
            container.name = layer.name;
            layer.container = container;
            _this.layers[layer.name] = layer;
            wi = layer.width;
            hi = layer.height;
            x = 0;
            y = 0;
            _ref1 = layer.data;
            _fn = function(key) {
              var tile;
              if (wi <= 0) {
                wi = layer.width;
                hi -= 1;
                y += _this.tileheight;
                x = 0;
              }
              tile = sprite.clone();
              tile.name = key;
              tile.x += x;
              tile.y += y;
              tile.gotoAndStop(key - 1);
              container.addChild(tile);
              x += _this.tilewidth;
              return wi -= 1;
            };
            for (_j = 0, _len1 = _ref1.length; _j < _len1; _j++) {
              key = _ref1[_j];
              _fn(key);
            }
            container.cache(0, 0, _this.width, _this.height);
            return _this.container.addChild(container);
          };
        })(this)(layer));
      }
      return _results;
    };

    return Tiles;

  })();
});
