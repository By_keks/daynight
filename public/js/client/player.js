var __hasProp = {}.hasOwnProperty,
  __extends = function(child, parent) { for (var key in parent) { if (__hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; };

define(["physics", "controls"], function(Physics, Controls) {
  var Player;
  return Player = (function(_super) {
    __extends(Player, _super);

    function Player(params, game, self) {
      this.game = game;
      this.self = self != null ? self : false;
      this.seters = {
        hp: (function(_this) {
          return function(val) {
            return _this.setHP(val);
          };
        })(this),
        pid: (function(_this) {
          return function(val) {
            return _this.pid = parseInt(val);
          };
        })(this),
        team: (function(_this) {
          return function(val) {
            return _this.team = parseInt(val);
          };
        })(this)
      };
      this.scale = 0.3;
      this.direction = 1;
      this.isAttacks = false;
      this.lock = false;
      Player.__super__.constructor.apply(this, arguments);
      this.parse(params);
      this.render();
      if (this.self) {
        this.delegateEvents();
      }
      createjs.Ticker.addEventListener("tick", ((function(_this) {
        return function(e) {
          return _this.update(e);
        };
      })(this)));
    }

    Player.prototype.parse = function(params) {
      return _.each(params, (function(_this) {
        return function(val, name) {
          if (_.has(_this.seters, name)) {
            return _this.seters[name](val);
          }
          _this[name] = val;
        };
      })(this));
    };

    Player.prototype.toJSON = function() {
      return {
        id: this.pid,
        x: this.x,
        y: this.y,
        direction: this.direction
      };
    };

    Player.prototype.setHP = function(val) {
      if (this.hp === val) {
        return;
      }
      if (!_.isUndefined(this.hp)) {
        this.hpChange(val);
      }
      this.hp = val;
      this.setName(this.name, false);
    };

    Player.prototype.hpChange = function(val) {
      var hp, text;
      hp = new createjs.Container;
      text = this.title.clone();
      if (this.hp > val) {
        text.text = "-" + (this.hp - val);
        text.color = "red";
      } else {
        text.text = "+" + (val - this.hp);
        text.color = "green";
      }
      text.x = 0;
      hp.addChild(text);
      this.addChild(hp);
      hp.cache(-20, -25, 100, 100);
      return createjs.Tween.get(hp).to({
        y: hp.y - 100
      }, 1000).call((function(_this) {
        return function() {
          return _this.removeChild(hp);
        };
      })(this));
    };

    Player.prototype.delegateEvents = function() {
      this.game.stage.on('stagemousedown', (function(_this) {
        return function(e) {
          _this.mouseX = _this.getXForWorld(parseInt(e.stageX));
          _this.mouseY = _this.getYForWorld(parseInt(e.stageY));
          return _this.attackOn();
        };
      })(this));
      return this.game.stage.on('stagemouseup', (function(_this) {
        return function(e) {
          return _this.attackOff();
        };
      })(this));
    };

    Player.prototype.spineComplete = function() {
      return this.spine.state.setAnimationByName(0, this.spine.state.data.skeletonData.animations[0].name, true);
    };

    Player.prototype.render = function() {
      this.spine = new createjs.Spine("res/sceletons/ninja/speedy.atlas", "res/sceletons/ninja/speedy.json");
      this.spine.scaleX = this.spine.scaleY = this.scale;
      this.spine.x = this.width / 2;
      this.spine.y = this.height;
      this.spine.onComplete = (function(_this) {
        return function() {
          return _this.spineComplete();
        };
      })(this);
      this.regX = this.width / 2;
      this.regY = this.height / 2;
      this.startX = this.x;
      this.startY = this.y;
      this.title = new createjs.Text("", "20px Arial", this.color);
      this.title.textBaseline = "alphabetic";
      this.setName(this.name);
      return this.addChild(this.spine, this.title);
    };

    Player.prototype.setName = function(name, change) {
      if (change == null) {
        change = true;
      }
      if (!this.title) {
        return;
      }
      if (change) {
        this.name = name;
      }
      this.title.text = "" + name + " HP:" + this.hp;
      this.title.x = -this.title.getMeasuredWidth() / 2 + this.width / 2;
      this.title.y = -10;
      return this.title.cache(-20, -20, 200, 300);
    };

    Player.prototype.setTarget = function(x, y) {
      var player, targets, _fn, _i, _len, _targets;
      targets = [];
      _targets = [];
      _.each(this.game.socketWrapper.players, (function(_this) {
        return function(player) {
          var deltaX, deltaY;
          if (player.team === _this.team) {
            return;
          }
          deltaX = _this.x - player.x;
          if (deltaX < 0) {
            deltaX *= -1;
          }
          deltaY = _this.y - player.y;
          if (deltaY < 0) {
            deltaY *= -1;
          }
          if (deltaX <= _this.attackRadiusX && deltaY <= _this.attackRadiusY) {
            return targets.push(player);
          }
        };
      })(this));
      _fn = (function(_this) {
        return function(player) {
          var deltaX, deltaY;
          deltaX = x - player.x;
          if (deltaX < 0) {
            deltaX *= -1;
          }
          deltaY = y - player.y;
          if (deltaY < 0) {
            deltaY *= -1;
          }
          return _targets.push({
            deltaX: deltaX,
            deltaY: deltaY,
            target: player
          });
        };
      })(this);
      for (_i = 0, _len = targets.length; _i < _len; _i++) {
        player = targets[_i];
        _fn(player);
      }
      _targets.sort((function(body1, body2) {
        var weight;
        weight = 0;
        weight += body1.deltaX < body2.deltaX ? -1 : body1.deltaX > body2.deltaX ? 1 : 0;
        weight += body1.deltaY < body2.deltaY ? -1 : body1.deltaY > body2.deltaY ? 1 : 0;
        return weight;
      }));
      return this.target = _targets.length > 0 ? _.first(_targets).target : null;
    };

    Player.prototype.attackOn = function() {
      return this.isAttacks = true;
    };

    Player.prototype.attackOff = function() {
      return this.isAttacks = false;
    };

    Player.prototype.sendAttack = function() {
      if (this.lockAttack) {
        return;
      }
      this.setTarget(this.mouseX, this.mousey);
      if (!this.target) {
        return;
      }
      return this.game.socketWrapper.send({
        cmd: "attk",
        params: {
          player: this.pid,
          target: this.target.pid
        }
      });
    };

    Player.prototype.destroy = function() {
      return this.game.world.removeChild(this);
    };

    Player.prototype.update = function(e) {
      if (this.isDead) {
        return;
      }
      if (this.isAttacks) {
        this.sendAttack();
      }
      this.spine.scaleX = this.scale * this.direction;
      this.spine.update(e);
      if (this.self) {
        return Player.__super__.update.call(this, e);
      }
    };

    return Player;

  })(Physics);
});
