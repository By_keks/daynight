define(["player"], function(Player) {
  var Socket;
  return Socket = (function() {
    function Socket(game) {
      var server;
      this.game = game;
      this.players = {};
      server = "ws://185.20.227.221:8001";
      this.socket = new WebSocket(server);
      this.socket.onopen = (function(_this) {
        return function() {
          console.log("Соединение установлено.");
          return _this.send({
            cmd: "signIn",
            params: {
              nick: localStorage.getItem("nick"),
              pid: null
            }
          });
        };
      })(this);
      this.socket.onclose = function(event) {
        if (event.wasClean) {
          console.log('Соединение закрыто чисто');
        } else {
          console.log('Обрыв соединения');
        }
        return console.log('Код: ' + event.code + ' причина: ' + event.reason);
      };
      this.socket.onmessage = (function(_this) {
        return function(event) {
          var data;
          data = JSON.parse(event.data);
          if (_.isFunction(_this[data.cmd])) {
            return _this[data.cmd](data.params);
          }
        };
      })(this);
      this.socket.onerror = function(error) {
        return console.log("Ошибка " + error.message);
      };
    }

    Socket.prototype.send = function(data) {
      return this.socket.send(JSON.stringify(data));
    };

    Socket.prototype.signIn = function(params) {
      if (params.status === "success") {
        return this.game.createWorld(params);
      }
    };

    Socket.prototype.signOut = function(params) {
      this.players[params.player.pid].destroy();
      return delete this.players[params.player.pid];
    };

    Socket.prototype.upd = function(params) {
      if (_.isUndefined(this.game.player)) {
        return;
      }
      return _.each(params.players, (function(_this) {
        return function(player) {
          var x, y;
          x = player.x;
          y = player.y;
          delete player.x;
          delete player.y;
          if (parseInt(player.pid) === parseInt(_this.game.player.pid)) {
            delete player.direction;
            delete player.color;
            delete player.team;
            return _this.game.player.parse(player);
          } else {
            if (!_.has(_this.players, player.pid)) {
              _this.players[player.pid] = new Player(player, _this.game, false);
              _this.game.world.addChild(_this.players[player.pid]);
            }
            _this.players[player.pid].parse(player);
            if (parseInt(_this.players[player.pid].x) === parseInt(x) && parseInt(_this.players[player.pid].y) === parseInt(y)) {
              return;
            }
            if (_this.players[player.pid].tween) {
              createjs.Tween.removeTweens(_this.players[player.pid]);
            }
            return _this.players[player.pid].tween = createjs.Tween.get(_this.players[player.pid]).to({
              x: x,
              y: y
            }, 50);
          }
        };
      })(this));
    };

    return Socket;

  })();
});
