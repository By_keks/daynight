var FPS, HEIGHT, SCALE, WIDTH;

requirejs.config({
  baseUrl: '/js/client/'
});

WIDTH = 1000;

HEIGHT = 500;

SCALE = 50;

FPS = 60;

$(function() {
  var init;
  init = function() {
    if (_.isNull(localStorage.getItem('nick'))) {
      $(".sign_in").show();
      $(".sign_in input:button").on('click', function(e) {
        if (_.isEmpty($("#nick").val())) {
          return $(".sign_in").css({
            color: "#F0142C"
          });
        }
        localStorage.setItem('nick', $("#nick").val());
        return init();
      });
      return true;
    } else {
      $(".sign_in").hide();
      return require(["game"], function(Game) {
        var canvas, game;
        canvas = $("<canvas id='canvas' unselectable='on' width='1200' height='750'></canvas>");
        game = new Game($('.wrapper').append(canvas).find('canvas')[0]);
        $(window).on('blur', (function(e) {
          return game.pause();
        }));
        $(window).on('focus', (function(e) {
          return game.play();
        }));
        document.body.appendChild(game.stats.domElement);
        game.stats.domElement.style.position = "absolute";
        game.stats.domElement.style.top = "0px";
        $("#fs").on('click', function(e) {
          e.preventDefault();
          return $("canvas")[0].mozRequestFullScreen();
        });
        canvas.attr({
          width: WIDTH,
          height: HEIGHT
        });
        return $('.wrapper').width(WIDTH);
      });
    }
  };
  return init();
});
