var Player;

module.exports = Player = (function() {
  function Player(connect) {
    this.connect = connect;
    this["default"] = {
      team: 0,
      hp: 100,
      width: 60,
      height: 90,
      damage: 20,
      color: "red",
      direction: 1,
      isDead: false,
      lockAttack: false,
      x: World.width / 2,
      y: World.height / 2,
      attackRadiusX: 80,
      attackRadiusY: 100
    };
    this.params = _.clone(this["default"]);
    this.isDead = false;
    this.timeoutToResp = 3000;
    this.lastAttack = 1000;
    this.target = null;
  }

  Player.prototype.toJSON = function() {
    return this.params;
  };

  Player.prototype.parse = function(params) {
    return _.each(params, (function(_this) {
      return function(val, name) {
        if (name === "pid") {
          parseInt(val);
        }
        return _this.params[name] = val;
      };
    })(this));
  };

  Player.prototype.getsHit = function(player) {
    var hp, impulseX, params;
    if (this.params.isDead) {
      return;
    }
    hp = this.params.hp;
    hp -= player.params.damage;
    if (hp <= 0) {
      this.params.isDead = true;
    }
    clearTimeout(this.timerToResp);
    if (this.params.isDead) {
      this.timerToResp = setTimeout(((function(_this) {
        return function() {
          return _this.resp();
        };
      })(this)), 5000);
    }
    this.params.hp = hp;
    impulseX = this.params.x > player.params.x ? 5 : this.params.x < player.params.x ? -5 : 0;
    params = _.extend(_.clone(this.toJSON()), {
      impulseX: impulseX,
      impulseY: -10
    });
    return this.connect.send({
      cmd: "upd",
      params: {
        players: [params]
      }
    });
  };

  Player.prototype.resp = function() {
    this.params.x = this["default"].x;
    this.params.y = this["default"].y;
    this.params.hp = this["default"].hp;
    return this.params.isDead = this["default"].isDead;
  };

  Player.prototype.attack = function(target) {
    if (this.params.lockAttack) {
      return;
    }
    target.getsHit(this);
    this.params.lockAttack = true;
    return setTimeout(((function(_this) {
      return function() {
        return _this.params.lockAttack = false;
      };
    })(this)), 1000);
  };

  Player.prototype.switchTeam = function() {
    this.params.team = this.params.team === 0 ? 1 : 0;
    return this.params.color = this.params.team === 0 ? "red" : "blue";
  };

  return Player;

})();
