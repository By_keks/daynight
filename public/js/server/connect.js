var Connect, Player;

Player = require("./player");

module.exports = Connect = (function() {
  Connect.prototype.params = {
    pid: 0,
    pids: [],
    teams: {
      0: 0,
      1: 0
    }
  };

  function Connect(conn, server) {
    this.server = server;
    console.log("New connection");
    this.open = true;
    this.conn = conn;
    this.conn.on("text", ((function(_this) {
      return function(str) {
        return _this.text(str);
      };
    })(this)));
    this.conn.on("close", ((function(_this) {
      return function() {
        return _this.close();
      };
    })(this)));
    this;
  }

  Connect.prototype.close = function() {
    var pid;
    pid = this.player.params.pid;
    this.params.teams[this.player.params.team] -= 1;
    console.log("Player " + pid + " goodbye");
    this.server.send({
      cmd: "signOut",
      params: {
        player: this.player.toJSON()
      }
    });
    this.server.players[pid] = null;
    return delete this.server.players[pid];
  };

  Connect.prototype.send = function(data) {
    return this.conn.sendText(JSON.stringify(data));
  };

  Connect.prototype.text = function(str) {
    var data;
    data = JSON.parse(str);
    if (_.isFunction(this[data.cmd])) {
      return this[data.cmd](data.params);
    }
  };

  Connect.prototype.signIn = function(params) {
    var pid;
    pid = _.isEmpty(params.pid) ? this.params.pid += 1 : params.pid;
    this.player = new Player(this);
    this.player.params.pid = pid;
    this.player.params.name = params.nick;
    this.server.players[this.player.params.pid] = this.player;
    if (this.params.teams[0] > this.params.teams[1]) {
      this.player.switchTeam();
    }
    this.params.teams[this.player.params.team] += 1;
    console.log("Player " + pid + " join to " + this.player.params.color + " team");
    return this.send({
      cmd: "signIn",
      params: {
        player: this.player.toJSON(),
        world: World.toJSON(),
        status: "success"
      }
    });
  };

  Connect.prototype.upd = function(params) {
    return this.player.parse(params.player);
  };

  Connect.prototype.attk = function(params) {
    return this.player.attack(this.server.players[params.target]);
  };

  return Connect;

})();
