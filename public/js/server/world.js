var World, fs;

fs = require('fs');

World = (function() {
  function World() {
    this.map = JSON.parse(fs.readFileSync('./../../res/map.json', 'utf8'));
    this.width = this.map.width * this.map.tilewidth;
    this.height = this.map.height * this.map.tileheight;
  }

  World.prototype.toJSON = function() {
    return this.map;
  };

  return World;

})();

module.exports = new World;
