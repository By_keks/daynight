var Connect, Server, ws;

ws = require("nodejs-websocket");

global._ = require("lodash");

global.World = require("./world");

Connect = require("./connect");

module.exports = Server = (function() {
  function Server() {
    this.players = {};
    this.server = ws.createServer(((function(_this) {
      return function(conn) {
        return _this.connection(conn);
      };
    })(this))).listen(8001);
    setInterval(((function(_this) {
      return function() {
        return _this.send({
          cmd: "upd",
          params: {
            players: _this.players
          }
        });
      };
    })(this)), 50);
  }

  Server.prototype.connection = function(conn) {
    return new Connect(conn, this);
  };

  Server.prototype.send = function(data) {
    return this.server.connections.forEach((function(_this) {
      return function(connect) {
        return connect.sendText(JSON.stringify(data));
      };
    })(this));
  };

  return Server;

})();
